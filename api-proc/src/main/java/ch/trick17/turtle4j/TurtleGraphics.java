package ch.trick17.turtle4j;

import java.util.stream.Stream;

public class TurtleGraphics {

    private static final String IMPL = "ch.trick17.turtle4j.impl.";

    private static final Class<?> modelClass;
    private static final Object model;

    static {
        try {
            modelClass = Class.forName(IMPL + "TurtleModel");
            model = modelClass.getConstructor().newInstance();
            var uiClass = Class.forName(IMPL + "TurtleGraphicsUI");
            var ui = uiClass.getMethod("getInstance").invoke(null);
            uiClass.getMethod("addTurtle", modelClass).invoke(ui, model);
        } catch (ReflectiveOperationException e) {
            throw new AssertionError("turtle4j library not properly configured", e);
        }
    }

    public static void forward(double steps) {
        call("forward", steps);
    }

    public static void back(double steps) {
        call("back", steps);
    }

    public static void left(double degrees) {
        call("left", degrees);
    }

    public static void right(double degrees) {
        call("right", degrees);
    }

    public static void penUp() {
        call("penUp");
    }

    public static void penDown() {
        call("penDown");
    }

    public static void setPenColor(String color) {
        call("setPenColor", color);
    }

    public static void setPenWidth(double width) {
        call("setPenWidth", width);
    }

    public static void setSpeed(double speed) {
        call("setSpeed", speed);
    }

    private static void call(String method, Object... args) {
        try {
            var argTypes = Stream.of(args)
                    .map(Object::getClass)
                    .map(type -> type == Double.class ? double.class : type)
                    .toArray(Class<?>[]::new);
            modelClass.getMethod(method, argTypes).invoke(model, args);
        } catch (ReflectiveOperationException e) {
            throw new AssertionError("turtle4j library not properly configured", e);
        }
    }
}
