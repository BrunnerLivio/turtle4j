package proc;

import java.util.List;
import java.util.Random;

import static ch.trick17.turtle4j.TurtleGraphics.*;

public class PenColor {

    public static void main(String[] args) {
        var random = new Random();
        var colors = List.of("crimson", "orange", "blue", "darkgreen");
        setSpeed(200);
        while (true) {
            var color = colors.get(random.nextInt(colors.size()));
            setPenColor(color);
            forward(10);
            right(random.nextInt(180) - 90);
        }
    }
}
