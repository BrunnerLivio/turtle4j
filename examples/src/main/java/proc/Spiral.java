package proc;

import static ch.trick17.turtle4j.TurtleGraphics.*;

public class Spiral {

    public static void main(String[] args) {
        setSpeed(500);
        for (int i = 0; i < 50; i++) {
            forward(50 + i * 2);
            right(90);
        }
    }
}
