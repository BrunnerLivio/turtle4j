package oop;

import ch.trick17.turtle4j.Turtle;

public class PenColorHsl {

    public static void main(String[] args) {
        var turtle = new Turtle();
        turtle.setSpeed(5000);
        for (int i = 0; i < 360; i++) {
            turtle.setPenColor("hsl(" + i + ", 80%, 90%)");
            turtle.forward(70);
            turtle.back(70);
            turtle.left(1);
        }
    }
}
