package oop;

import ch.trick17.turtle4j.Turtle;

public class PenUpDown {

    public static void main(String[] args) {
        var turtle = new Turtle();
        turtle.setSpeed(2000);
        for (int i = 0; i < 20; i++) {
            dashedLine(turtle, 2 + i);
            turtle.right(90);
        }
    }

    public static void dashedLine(Turtle turtle, int dashes) {
        for (int i = 1; i < dashes; i++) {
            turtle.forward(8);
            turtle.penUp();
            turtle.forward(8);
            turtle.penDown();
        }
        turtle.forward(8);
    }
}
