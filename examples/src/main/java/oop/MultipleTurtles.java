package oop;

import ch.trick17.turtle4j.Turtle;

import java.util.ArrayList;

public class MultipleTurtles {

    public static void main(String[] args) {
        var n = 11;

        var turtles = new ArrayList<Turtle>();
        for (int i = 0; i < n; i++) {
            var t = new Turtle();
            t.setSpeed(1000);
            t.right(i * 360.0 / n);
            turtles.add(t);
        }

        for (int i = 0; i < 360; i++) {
            turtles.forEach(t -> t.forward(1));
            turtles.forEach(t -> t.right(1));
        }
    }
}
