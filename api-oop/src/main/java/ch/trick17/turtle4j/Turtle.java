package ch.trick17.turtle4j;

import java.util.stream.Stream;

public class Turtle {

    private static final String IMPL = "ch.trick17.turtle4j.impl.";

    private final Class<?> modelClass;
    private final Object model;

    public Turtle() {
        try {
            modelClass = Class.forName(IMPL + "TurtleModel");
            model = modelClass.getConstructor().newInstance();
            var uiClass = Class.forName(IMPL + "TurtleGraphicsUI");
            var ui = uiClass.getMethod("getInstance").invoke(null);
            uiClass.getMethod("addTurtle", modelClass).invoke(ui, model);
        } catch (ReflectiveOperationException e) {
            throw new AssertionError("turtle4j library not properly configured", e);
        }
    }

    public void forward(double steps) {
        call("forward", steps);
    }

    public void back(double steps) {
        call("back", steps);
    }

    public void left(double degrees) {
        call("left", degrees);
    }

    public void right(double degrees) {
        call("right", degrees);
    }

    public void penUp() {
        call("penUp");
    }

    public void penDown() {
        call("penDown");
    }

    public void setPenColor(String color) {
        call("setPenColor", color);
    }

    public void setPenWidth(double width) {
        call("setPenWidth", width);
    }

    public void setSpeed(double speed) {
        call("setSpeed", speed);
    }

    private void call(String method, Object... args) {
        try {
            var argTypes = Stream.of(args)
                    .map(Object::getClass)
                    .map(type -> type == Double.class ? double.class : type)
                    .toArray(Class<?>[]::new);
            modelClass.getMethod(method, argTypes).invoke(model, args);
        } catch (ReflectiveOperationException e) {
            throw new AssertionError("turtle4j library not properly configured", e);
        }
    }
}
