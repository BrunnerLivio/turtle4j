package ch.trick17.turtle4j.impl;

import javafx.animation.Transition;
import javafx.util.Duration;

import java.util.function.DoubleConsumer;

class CustomTransition extends Transition {

    private final DoubleConsumer action;

    public CustomTransition(Duration duration, DoubleConsumer action) {
        this.action = action;
        setCycleDuration(duration);
    }

    protected void interpolate(double frac) {
        action.accept(frac);
    }
}
