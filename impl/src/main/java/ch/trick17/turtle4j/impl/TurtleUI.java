package ch.trick17.turtle4j.impl;

import javafx.scene.Group;
import javafx.scene.image.ImageView;

class TurtleUI extends Group {

    public TurtleUI() {
        var view = new ImageView("/turtle.png");
        view.setRotate(90);
        view.setPreserveRatio(true);
        view.setFitWidth(25);
        view.setSmooth(true);
        getChildren().add(view);
    }
}
