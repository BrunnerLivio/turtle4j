package ch.trick17.turtle4j.impl;

import javafx.animation.Animation;

import java.util.concurrent.CountDownLatch;

/**
 * Simple latch that always counts down from 1 and has a {@link #await()}
 * method that does not throw an {@link InterruptedException}. Also, it
 * can conveniently be used with {@link Animation}s,
 * through {@link #finishAfter(Animation)}.
 */
class SimpleLatch extends CountDownLatch {

    public SimpleLatch() {
        super(1);
    }

    public void await() {
        try {
            super.await();
        } catch (InterruptedException ignored) {}
    }

    /**
     * Plays the given animation and counts down as soon it is
     * finished
     */
    public void finishAfter(Animation animation) {
        animation.setOnFinished(e -> countDown());
        animation.play();
    }
}
